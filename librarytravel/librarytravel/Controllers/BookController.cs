﻿using BusinessLogic.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Session;
using BusinessLogic.Objects;

namespace librarytravel.Controllers
{
    public class BookController : Controller
    {

        private IBook Book;
        public BookController(IBook Book) 
        {
            this.Book = Book;
        }
        
        [HttpGet]
        public IActionResult GetBooks()
        {
            try
            {
                var Book = this.Book.GetBooks();
                return Ok(new {response=Book, message="OK" });
            }
            catch(Exception e) 
            {
              return Ok(new {message=e.Message.ToString() });
            }
        }

        [HttpGet]
        public IActionResult GetBook(int bookid) 
        {
            try
            {
                var book = this.Book.GetBook(bookid);
                return Ok(new {response=book, message="OK"});
            }
            catch(Exception e) 
            {
                return Ok(new { message = e.Message.ToString() });
            }
        }

        [HttpPost]
        public IActionResult Autentication(ObjUser user) 
        {

            return null;
        }
    }
}
