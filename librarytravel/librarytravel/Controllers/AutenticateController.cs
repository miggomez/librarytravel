﻿using BusinessLogic.Interfaces;
using BusinessLogic.Objects;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace librarytravel.Controllers
{
    public class AutenticateController : Controller
    {

        private IAutenticate autenticate;

        public AutenticateController(IAutenticate autenticate) 
        {
            this.autenticate = autenticate;
        }

        [HttpPost]
        public IActionResult GetUser([FromBody]ObjUser user) 
        {
            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user")))
                {
                    return Ok(new { response = true });
                }
                else 
                {
                    var res = this.autenticate.GetUser(user.Email, user.Password);
                    if (res != null)
                    {
                        HttpContext.Session.SetString("user", res.Email);
                        return Ok(new { response = true, user=res.Email });
                    }
                    else
                    {
                        return Ok(new { response = false });
                    }
                }              
            }
            catch (Exception e) 
            {
                return Ok(new {message=e.Message.ToString() });
            }
        }

        [HttpPost]
        public IActionResult Save(ObjUser user) 
        {
            try
            {
                var res = this.autenticate.Save(user);
                return Ok(new {response=res });
            }
            catch (Exception e) 
            {
                return Ok(new {message=e.Message.ToString() });
            }
        }
    }
}
