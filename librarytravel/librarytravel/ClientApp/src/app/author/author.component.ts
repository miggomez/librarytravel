import { Component, OnInit } from '@angular/core';
import { AutenticateService } from 'src/app/services/autenticate.service';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {

  constructor(private autService: AutenticateService)
  {
    this.GetUser();
  }

  ngOnInit() {
  }

  email: string;
  password: string;
  data: any;

  GetUser() {
 
    let user =
    {
      Email: this.email,
      Password: this.password
    }
    this.autService.GetUser(user).subscribe(res => {
      this.data = res;
      if (this.data.response) {
        location.href = '/book'
      }
      else {
        alert("usuario o clave invalida");
        location.href = '/'
      }

    });;
  }

}
