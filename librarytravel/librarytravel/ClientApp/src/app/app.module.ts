import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { BookComponent } from './book/book.component';
import { EditorialComponent } from './editorial/editorial.component';
import { AuthorComponent } from './author/author.component';
import { AutenticateComponent } from './autenticate/autenticate.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    BookComponent,
    EditorialComponent,
    AuthorComponent,
    AutenticateComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: AutenticateComponent, pathMatch: 'full' },
      { path: 'editorial', component: EditorialComponent },
      { path: 'author', component: AuthorComponent },
      { path: 'book', component: BookComponent },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
