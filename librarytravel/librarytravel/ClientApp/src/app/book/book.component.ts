import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/services/book.service';
import { AutenticateService } from 'src/app/services/autenticate.service';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  Books: any;
  Book: any;
  data: any;
  popup: boolean;
  email: string;
  password: string;

  constructor(private book: BookService, private autService: AutenticateService)
  {
    debugger
    if (window.localStorage.getItem("user") == "1")
    {
      
    }
    else
    {
      this.GetUser();
    }
    this.popup = false;
    this.GetBooks();
   
  }

  ngOnInit()
  {
  }

  GetBook(bookid: number)
  {
    debugger
    this.Book = this.book.GetBook(bookid).subscribe(res => {
      this.Book = res;
      this.popup = true;
    });
  }

  GetBooks()
  {
    debugger
    this.Books = this.book.GetBooks().subscribe(res => {
      this.Books = res
    });
  }

  GetUser()
  {
    
    let user =
    {
      Email: this.email,
      Password: this.password
    }
    this.autService.GetUser(user).subscribe(res => {
      this.data = res;
      if (this.data.response) {
        window.localStorage.setItem("user", "1");
        location.href = '/book'
      }
      else
      {
        alert("usuario o clave invalida");
        location.href = '/'
        window.localStorage.removeItem("user");
      }

    });;
  }

  ClosePopUp()
  {
    this.popup = false;

  }



}



interface Book {
  Isbn: number;
  Editorial_id: number;
  Title: string;
  Sinopsis: string;
  Pages: string;
}
