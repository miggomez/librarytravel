import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Inject } from '@angular/core';
import { AutenticateComponent } from '../autenticate/autenticate.component';

@Injectable({
  providedIn: 'root'
})
export class AutenticateService {


  url: string;
  data: any;
  isUser: boolean;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string)
  {
    this.url = baseUrl;
  }

  GetUser(user: any)
  {
    return this.http.post(this.url + 'Autenticate/GetUser', user);
  }

  Save(user: any)
  {
    return this.http.post(this.url + 'Autenticate/Save', user);
  }
}
