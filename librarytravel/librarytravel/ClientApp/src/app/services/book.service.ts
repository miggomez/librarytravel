import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Inject } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  url: string;
  data: any;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string)
  {
    this.url = baseUrl;
  }

  GetBook(bookid: number)
  {
    return this.http.get(this.url + 'Book/GetBook?bookid=' + bookid);
  }

  GetBooks()
  {
    return this.http.get(this.url + 'Book/GetBooks');
  }

}



