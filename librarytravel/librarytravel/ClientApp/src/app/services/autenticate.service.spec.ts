import { TestBed } from '@angular/core/testing';

import { AutenticateService } from './autenticate.service';

describe('AutenticateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutenticateService = TestBed.get(AutenticateService);
    expect(service).toBeTruthy();
  });
});
