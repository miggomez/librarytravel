import { Component, OnInit } from '@angular/core';
import { AutenticateService } from 'src/app/services/autenticate.service';

@Component({
  selector: 'app-autenticate',
  templateUrl: './autenticate.component.html',
  styleUrls: ['./autenticate.component.css']
})
export class AutenticateComponent implements OnInit {

  constructor(private autService: AutenticateService) { }

  ngOnInit() {
  }

  email: string;
  password: string;
  data: any;

  GetUser() {
    debugger
    let user =
    {
      Email: this.email,
      Password: this.password
    }
    this.autService.GetUser(user).subscribe(res => {
      this.data = res;
      if (this.data.response) {
        location.href = '/book'
      }
      else {
        alert("usuario o clave invalida");
        location.href = '/autenticate'
      }

    });;
  }

}
