﻿using BusinessLogic.Business;
using BusinessLogic.Objects;
using DataModel.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLibraryTravel.BusinessLogicTest
{

    public class BookTest
    {

        private LibraryTravelContext context = new LibraryTravelContext();

        [Test]
        public void GetBooksTest() 
        {
            var books = context.Libros.ToList();
            Assert.IsNotNull(books);
        }

        [Test]
        public void GetBookTest() 
        {
            int bookid = 1;
            Book book = new Book();
            ObjBook _book =book.GetBook(bookid);
            Assert.AreEqual(bookid, _book.Isbn);
        }

        [Test]
        public void GetEditorialNameTest() 
        {
            int editorialId = 2;
            string editorialName = "antioqueña de libros";
            var editorial = context.Editoriales.Find(editorialId);
            Assert.AreEqual(editorial.Nombre,editorialName);

        }

    }
}
