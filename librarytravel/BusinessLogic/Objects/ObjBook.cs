﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Objects
{
    public class ObjBook
    {
        public int Isbn { get; set; }
        public string Editorial { get; set; }
        public string Title { get; set; }
        public string Sinopsis { get; set; }
        public string Pages { get; set; }

    }
}
