﻿using BusinessLogic.Interfaces;
using BusinessLogic.Objects;
using DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class User : IAutenticate
    {

        private LibraryTravelContext context = new LibraryTravelContext();

        public bool Save(ObjUser user)
        {
            Usuario res = (from u in context.Usuarios where u.Email==user.Email select u).FirstOrDefault();
            if (res == null)
            {
                Usuario _user = new Usuario
                {
                    Password = user.Password,
                    Cedula = user.Identification,
                    Nombre = user.Name,
                    Email = user.Email

                };
                context.Usuarios.Add(_user);
                context.SaveChanges();
                return true;
            }
            else 
            {
                return false;
            }

        }
        public ObjUser GetUser(string email, string password)
        {
            var user = (from u in context.Usuarios
                        where u.Email == email && u.Password == password
                        select u).FirstOrDefault();

            ObjUser _user = null;

            if (user !=null) 
            {
                _user = new ObjUser
                {
                    Email = user.Email,
                    Identification = user.Cedula,
                    Name = user.Nombre,
                    Password = user.Password
                };
            }
        return _user;
        }

    }
}
