﻿using BusinessLogic.Interfaces;
using BusinessLogic.Objects;
using DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Business
{
    public class Book : IBook
    {
        private LibraryTravelContext context = new LibraryTravelContext();
        public ObjBook GetBook(int bookid)
        {
            var book = context.Libros.Find(bookid);
            ObjBook Book = new ObjBook
            {
                Editorial = GetEditorialName(book.EditorialesId),
                Isbn = book.Isbn,
                Pages = book.NPaginas,
                Sinopsis = book.Sinopsis,
                Title = book.Titulo
            };

            return Book;
        }

        public List<ObjBook> GetBooks()
        {
            var books = context.Libros.ToList();
            List<ObjBook> _books = new List<ObjBook>();
            foreach (var book in books) 
            {
                _books.Add(new ObjBook
                {
                    Editorial = GetEditorialName(book.EditorialesId),
                    Isbn=book.Isbn,
                    Pages=book.NPaginas,
                    Sinopsis=book.Sinopsis,
                    Title=book.Titulo
                });
            }

            return _books;
        }

        public string GetEditorialName(int editorialid) 
        {
            var editorial = context.Editoriales.FirstOrDefault(x=> x.Id==editorialid).Nombre;
            return editorial;
        }
    }
}
