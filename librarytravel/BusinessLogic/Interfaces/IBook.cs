﻿using BusinessLogic.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IBook
    {
      public List<ObjBook> GetBooks();
      public ObjBook GetBook(int bookid);
      public string GetEditorialName(int editorialid);
    
    }
}
