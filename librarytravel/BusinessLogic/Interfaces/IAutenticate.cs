﻿using BusinessLogic.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
   public interface IAutenticate
    {
        public ObjUser GetUser(string email, string password);
        public bool Save(ObjUser user);
    }
}
